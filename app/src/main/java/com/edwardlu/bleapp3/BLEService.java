package com.edwardlu.bleapp3;

import android.Manifest;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;

/*
 * Adapted from:
 * http://developer.android.com/samples/BluetoothLeGatt/src/com.example.android.bluetoothlegatt/BluetoothLeService.html
 */

public class BLEService extends Service {
    private final static String TAG = BLEService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothGattService mBluetoothGattService;

    // number of non colon characters in a MAC address
    // the reason I have this is to get rid of the colons from the mac address when sending connect
    // interests because for some reason I can't put colons in interest names
    private final int MAC_ADDRESS_LENGTH_NO_COLONS = 12;

    public final static String ACTION_CONNECTING =
            "com.bleService.ACTION_CONNECTING";
    public final static String ACTION_DISCOVERING_SERVICES =
            "com.bleService.ACTION_DISCOVERING_SERVICES";
    public final static String ACTION_CONNECTED =
            "com.bleService.ACTION_CONNECTED";
    public final static String ACTION_DISCONNECTED =
            "com.bleService.ACTION_DISCONNECTED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.bleService.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.bleService.EXTRA_DATA";
    public final static String UUID_LIST =
            "com.bleService.UUID_LIST";
    public final static String MAC_ADDRESS_NO_COLONS =
            "com.bleService.MAC_ADDRESS";
    public final static String CONNECTION_INFORMATION =
            "com.bleService.CONNECTION_INFORMATION";

    public static String shortUuidFormat = "0000%04X-0000-1000-8000-00805F9B34FB";

    static UUID sixteenBitUuid(long shortUuid) {
        assert shortUuid >= 0 && shortUuid <= 0xFFFF;
        return UUID.fromString(String.format(shortUuidFormat, shortUuid & 0xFFFF));
    }

    public final static UUID UUID_SERVICE = sixteenBitUuid(0x2220);
    public final static UUID UUID_HMSOFT = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_RECEIVE = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_SEND = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_DISCONNECT = sixteenBitUuid(0x2223);
    public final static UUID UUID_CLIENT_CONFIGURATION = sixteenBitUuid(0x2902);

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            String noColonsInMACAddress = removeColonsFromMACAddress(gatt.getDevice().getAddress());

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "Connected to BLE Device.");

                Intent discoveringServicesIntent = new Intent(ACTION_DISCOVERING_SERVICES);

                discoveringServicesIntent.putExtra(MAC_ADDRESS_NO_COLONS, noColonsInMACAddress);
                discoveringServicesIntent.putExtra(ACTION_DISCOVERING_SERVICES, noColonsInMACAddress);

                sendBroadcast(discoveringServicesIntent);

                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());


            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from BLE device.");

                Intent disconnectedIntent = new Intent(ACTION_DISCONNECTED);

                disconnectedIntent.putExtra(MAC_ADDRESS_NO_COLONS, noColonsInMACAddress);
                disconnectedIntent.putExtra(ACTION_DISCONNECTED, noColonsInMACAddress);

                sendBroadcast(disconnectedIntent);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                List<BluetoothGattService> discoveredServices = gatt.getServices();
                String discoveredServicesString = "";

                for (BluetoothGattService service : discoveredServices) {
                    String serviceUUID = "SUUID: " + service.getUuid().toString();

                    List<BluetoothGattCharacteristic> discoveredCharacteristics = service.getCharacteristics();

                    discoveredServicesString += serviceUUID + "\n";

                    for (BluetoothGattCharacteristic characteristic : discoveredCharacteristics) {

                        String characteristicUUID = "   CUUID: "  + characteristic.getUuid().toString();

                        int properties = characteristic.getProperties();

                        String propertiesString = checkCharacteristicProperties(properties);

                        List<BluetoothGattDescriptor> discoveredDescriptors = characteristic.getDescriptors();

                        discoveredServicesString += characteristicUUID + "\n" + "    " + propertiesString + "\n";

                        for (BluetoothGattDescriptor descriptor : discoveredDescriptors) {

                            String descriptorUUID = "      DUUID: " + descriptor.getUuid().toString();

                            int permissions = descriptor.getPermissions();

                            //String permissionsString = checkDescriptorPermissions(permissions);

                            discoveredServicesString += descriptorUUID + "\n";// + "       " + permissionsString + "\n";

                        }

                    }
                }

                Intent connectedIntent = new Intent(ACTION_CONNECTED);

                String[] information = new String[2];
                information[0] = discoveredServicesString;
                information[1] = removeColonsFromMACAddress(gatt.getDevice().getAddress());

                connectedIntent.putExtra(CONNECTION_INFORMATION, information);

                sendBroadcast(connectedIntent);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }



        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "there was a change in the BTLE module's characteristic");
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }



    };

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        if (UUID_RECEIVE.equals(characteristic.getUuid())) {
            final Intent intent = new Intent(action);
            intent.putExtra(EXTRA_DATA, characteristic.getValue());
            sendBroadcast(intent, Manifest.permission.BLUETOOTH);
        }
    }

    public class LocalBinder extends Binder {
        BLEService getService() {
            return BLEService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {

        Log.d(TAG, "entering the connect function of bleService");

        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            boolean connectionAttemptSucceeded = mBluetoothGatt.connect();
            if (connectionAttemptSucceeded) {
                Intent connectingIntent = new Intent(ACTION_CONNECTING);

                String noColonsInMACAddress = removeColonsFromMACAddress(address);
                connectingIntent.putExtra(MAC_ADDRESS_NO_COLONS, noColonsInMACAddress);

                Log.d(TAG, "no colon mac address" + noColonsInMACAddress);

                sendBroadcast(connectingIntent);
            }
            return connectionAttemptSucceeded;
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        Log.d(TAG, "Trying to connect to remote device with the address: " + address);
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;

        Intent connectingIntent = new Intent(ACTION_CONNECTING);

        String noColonsInMACAddress = removeColonsFromMACAddress(address);
        connectingIntent.putExtra(MAC_ADDRESS_NO_COLONS, noColonsInMACAddress);

        Log.d(TAG, "no colon mac address" + noColonsInMACAddress);

        sendBroadcast(connectingIntent);

        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public void read() {
        if (mBluetoothGatt == null || mBluetoothGattService == null) {
            Log.w(TAG, "BluetoothGatt not initialized");
            return;
        }

        BluetoothGattCharacteristic characteristic =
                mBluetoothGattService.getCharacteristic(UUID_RECEIVE);

        mBluetoothGatt.readCharacteristic(characteristic);
    }

    public boolean send(byte[] data) {
        if (mBluetoothGatt == null || mBluetoothGattService == null) {
            Log.w(TAG, "BluetoothGatt not initialized");
            return false;
        }

        BluetoothGattCharacteristic characteristic =
                mBluetoothGattService.getCharacteristic(UUID_SEND);

        if (characteristic == null) {
            Log.w(TAG, "Send characteristic not found");
            return false;
        }

        characteristic.setValue(data);
        characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        return mBluetoothGatt.writeCharacteristic(characteristic);
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_CONNECTED);
        filter.addAction(ACTION_DISCONNECTED);
        filter.addAction(ACTION_DATA_AVAILABLE);
        filter.addAction(ACTION_CONNECTING);
        filter.addAction(ACTION_DISCOVERING_SERVICES);
        return filter;
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    String checkCharacteristicProperties(int properties) {
        String propertiesString = "";

        if ((properties & BluetoothGattCharacteristic.PROPERTY_BROADCAST) > 0) {
            propertiesString += "B-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_EXTENDED_PROPS) > 0) {
            propertiesString += "EP-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0) {
            propertiesString += "I-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
            propertiesString += "N-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
            propertiesString += "R-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE) > 0) {
            propertiesString += "SW-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
            propertiesString += "W-";
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) > 0) {
            propertiesString += "WNR-";
        }

        return propertiesString;
    }

    // apparently this doesn't even work, like the descriptor permissions are meaningless and we're just supposed
    // to assume we can read from all descriptors? I guess that makes sense, why wouldn't you be able to...
    // but then again what about the write permission? eh ill figure it out tomorrow I guess...
    /*
    String checkDescriptorPermissions(int permissions) {
        String permissionsString = "";

        if ((permissions & BluetoothGattDescriptor.PERMISSION_READ) > 0) {
            permissionsString += "R-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_READ_ENCRYPTED) > 0) {
            permissionsString += "RE-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_READ_ENCRYPTED_MITM) > 0) {
            permissionsString += "REM-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_WRITE) > 0) {
            permissionsString += "W-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_WRITE_ENCRYPTED) > 0) {
            permissionsString += "WE-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_WRITE_ENCRYPTED_MITM) > 0) {
            permissionsString += "WEM-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_WRITE_SIGNED) > 0) {
            permissionsString += "WS-";
        }
        if ((permissions & BluetoothGattDescriptor.PERMISSION_WRITE_SIGNED_MITM) > 0) {
            permissionsString += "WSM-";
        }

        return permissionsString;
    }
    */

    String removeColonsFromMACAddress(String address) {
        StringBuilder sb = new StringBuilder(MAC_ADDRESS_LENGTH_NO_COLONS);

        sb.append(address.substring(0,2));
        sb.append(address.substring(3,5));
        sb.append(address.substring(6,8));
        sb.append(address.substring(9,11));
        sb.append(address.substring(12,14));
        sb.append(address.substring(15,17));

        String noColons = sb.toString();

        Log.d("MAC string builder ", noColons);

        return noColons;
    }
}
