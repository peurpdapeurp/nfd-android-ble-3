package com.edwardlu.bleapp3;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.InterestFilter;
import net.named_data.jndn.Name;
import net.named_data.jndn.NetworkNack;
import net.named_data.jndn.OnData;
import net.named_data.jndn.OnInterestCallback;
import net.named_data.jndn.OnNetworkNack;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.OnTimeout;
import net.named_data.jndn.encrypt.EncryptError;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.identity.IdentityManager;
import net.named_data.jndn.security.identity.MemoryIdentityStorage;
import net.named_data.jndn.security.identity.MemoryPrivateKeyStorage;
import net.named_data.jndn.util.Blob;
import net.named_data.jndn.util.SegmentFetcher;

import java.io.IOException;


public class NFDService extends Service {

    // this is the number of characters in a MAC address including colons
    private final int MAC_ADDRESS_LENGTH = 17;

    private static String TAG = "NFDService";

    // strings for the intent filter
    public final static String NFD_STOPPED = "NFD_STOPPED";
    public final static String FACE_CLOSED = "FACE_CLOSED";
    public final static String INTEREST_RECEIVED = "INTEREST_RECEIVED";
    public final static String INTEREST_DISCOVER_RECEIVED = "INTEREST_DISCOVER_RECEIVED";
    public final static String INTEREST_CONNECT_GET_SERVICES_RECEIVED = "INTEREST_CONNECT_RECEIVED";
    public final static String DATA_RECEIVED = "DATA_RECEIVED";
    public final static String INTEREST_SENT = "INTEREST_SENT";
    public final static String DATA_SENT = "DATA_SENT";
    public final static String INTEREST_TIMEOUT = "INTEREST_TIMEOUT";
    public final static String INTEREST_NACK = "INTEREST_NACK";
    public final static String INTEREST_DISCONNECT_RECEIVED = "INTEREST_DISCONNECT_RECEIVED";

    // strings for intent extras
    public final static String INTEREST_NAME = "INTEREST_NAME";
    public final static String INTEREST_MAC = "INTEREST_CONNECT_MAC";

    // strings for adding interest names to intents
    public final static String EXTRA_NAME = "EXTRA_NAME";

    private Face face;
    private KeyChain keyChain;
    private boolean alreadyRunning;

    /*
    public static Queue<Integer> receivedSequenceNumbers = new LinkedList<Integer>();

    // corresponds to the maximum number of outstanding interests for connection status by sending interest app
    public final static int MAX_SEQUENCE_QUEUE_SIZE = 50;
    */

    /*
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {



        return START_STICKY;
    }
    */

    public void createNFDTunnel() {
        if (!alreadyRunning) {
            networkThread.start();
        }
    }

    /*
    // sends string status data to NFD face, using the name passed in for the data name, appends sequence number
    public void sendStatusDataToNFDFace(String data, String prefix) {

        Log.d(TAG, "we entered the sendStatusDataToNFDFace function with the extra prefix information");

        String sequenceNumber = "";

        if (receivedSequenceNumbers.size() == 0) {

        } else {
            sequenceNumber = Integer.toString(receivedSequenceNumbers.poll());
        }

        Name dataName = new Name(prefix + "/" + sequenceNumber);

        Log.d(TAG, dataName.toString());

        Data sendData = new Data();
        sendData.setName(dataName);
        Blob content = new Blob(data.toString());
        sendData.setContent(content);

        Log.d(TAG, "expressed status data with name : " + sendData.getName().toString());
        Log.d(TAG, "expressed status data for state: " + content.toString());

        class OneShotTask implements Runnable {
            Data data;
            OneShotTask(Data d) { data = d; }
            public void run() {
                try {
                    face.putData(data);
                    Log.d(TAG, data.getName().toString());
                } catch (IOException e) {
                    Log.d(TAG, "failure when responding to data interest: " + e.toString());
                }
            }
        }
        Thread t = new Thread(new OneShotTask(sendData));
        t.start();

        Log.d(TAG, "printing out numbers in queue after expressing status data");
        for (int i : receivedSequenceNumbers) {
            Log.d(TAG, Integer.toString(i));
        }

    }
    */

    // sends string data to NFD face, using the name passed in for the data name
    public void sendDataToNFDFace(String data, String prefix) {

        Log.d(TAG, "we entered the sendDataToNFDFace function with the extra prefix information");

        Name dataName = new Name(prefix);

        Log.d(TAG, dataName.toString());

        Data sendData = new Data();
        sendData.setName(dataName);
        Blob content = new Blob(data.toString());
        sendData.setContent(content);

        Log.d(TAG, content.toString());

        class OneShotTask implements Runnable {
            Data data;
            OneShotTask(Data d) { data = d; }
            public void run() {
                try {
                    face.putData(data);
                } catch (IOException e) {
                    Log.d(TAG, "failure when responding to data interest: " + e.toString());
                }
            }
        }
        Thread t = new Thread(new OneShotTask(sendData));
        t.start();

    }

    // sends Data to NFD face, using the name passed in for the data name
    public void sendDataToNFDFace(byte[] data, String prefix) {

        Name dataName = new Name(prefix);

        Log.d(TAG, dataName.toString());

        Data sendData = new Data();
        sendData.setName(dataName);
        Blob content = new Blob(data);
        sendData.setContent(content);

        class OneShotTask implements Runnable {
            Data data;
            OneShotTask(Data d) { data = d; }
            public void run() {
                try {
                    face.putData(data);
                } catch (IOException e) {
                    Log.d(TAG, "failure when responding to data interest: " + e.toString());
                }
            }
        }
        Thread t = new Thread(new OneShotTask(sendData));
        t.start();

    }

    // sends Data to NFD face, using the name of the first interest from the queue
    public void sendDataToNFDFace(byte[] data) {

        Log.d(TAG, "sendDatatoNFDFace was successfully called");

        Name dataName = new Name("gibberish");

        if (dataName == null) {
            Log.d(TAG, "couldn't get name for data");
            return;
        } else {
            Log.d(TAG, dataName.toString());
        }


        Data sendData = new Data();
        sendData.setName(dataName);
        Blob content = new Blob(data);
        sendData.setContent(content);

        class OneShotTask implements Runnable {
            Data data;
            OneShotTask(Data d) { data = d; }
            public void run() {
                try {
                    face.putData(data);
                } catch (IOException e) {
                    Log.d(TAG, "failure when responding to data interest: " + e.toString());
                }
            }
        }
        Thread t = new Thread(new OneShotTask(sendData));
        t.start();

    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "we got told to destroy ourselves");
        super.onDestroy();
        face.shutdown();
        networkThread.interrupt();
    }

    private void registerDataPrefix () {
        Log.d(TAG, "registering data prefix...");
        try {
            face.registerPrefix(new Name(getString(R.string.baseName) + "/discover"), OnInterestDiscovery,
                    OnPrefixRegisterFailedDiscovery, OnPrefixRegisterSuccessDiscovery);
            face.registerPrefix(new Name(getString(R.string.baseName) + "/connect"), OnInterestConnect,
                    OnPrefixRegisterFailedConnect, OnPrefixRegisterSuccessConnect);
            face.registerPrefix(new Name(getString(R.string.baseName) + "/disconnect"), OnInterestDisconnect,
                    OnPrefixRegisterFailedDisconnect, OnPrefixRegisterSuccessDisconnect);
        } catch (IOException | SecurityException e) {
            // should also be handled in callback, but in just in case...
        }
    }


    private final OnInterestCallback OnInterestDiscovery = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId,
                               InterestFilter filterData) {

            Log.d(TAG, "we got an interest to discover devices");

            Intent interestDiscoverReceivedIntent = new Intent(INTEREST_DISCOVER_RECEIVED);
            sendBroadcast(interestDiscoverReceivedIntent);

        }
    };

    private final OnRegisterSuccess OnPrefixRegisterSuccessDiscovery = new OnRegisterSuccess() {
        @Override
        public void onRegisterSuccess(Name prefix, long registeredPrefixId) {
            Log.d(TAG, "successfully registered data prefix: " + prefix);
        }
    };

    private final OnRegisterFailed OnPrefixRegisterFailedDiscovery = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            Log.d(TAG, "we failed to register the data prefix: " + prefix);
        }
    };

    private final OnInterestCallback OnInterestDisconnect = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId,
                               InterestFilter filterData) {

            Log.d(TAG, "we got an interest to disconnect");

            String interestName = interest.getName().toString();

            String macAddressNoColons = interestName.substring(interestName.lastIndexOf('/') + 1);

            Intent interestConnectReceivedIntent = new Intent(INTEREST_DISCONNECT_RECEIVED);
            interestConnectReceivedIntent.putExtra(INTEREST_MAC, addColonsToMACAddress(macAddressNoColons));

            sendBroadcast(interestConnectReceivedIntent);

        }
    };

    private final OnRegisterSuccess OnPrefixRegisterSuccessDisconnect = new OnRegisterSuccess() {
        @Override
        public void onRegisterSuccess(Name prefix, long registeredPrefixId) {
            Log.d(TAG, "successfully registered data prefix: " + prefix);
        }
    };

    private final OnRegisterFailed OnPrefixRegisterFailedDisconnect = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            Log.d(TAG, "we failed to register the data prefix: " + prefix);
        }
    };

    private final OnInterestCallback OnInterestConnect = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId,
                               InterestFilter filterData) {

            String interestName = interest.getName().toString();

            if (interestName.contains("getServices")) {

                Log.d(TAG, "we got an interest to connect to a device");

                String macAddressNoColons = interestName.substring(interestName.lastIndexOf('/') - 12,
                        interestName.lastIndexOf('/'));

                Log.d(TAG, "macAddressNoColons: " + macAddressNoColons);

                Intent interestConnectGetServicesReceivedIntent = new Intent(INTEREST_CONNECT_GET_SERVICES_RECEIVED);
                interestConnectGetServicesReceivedIntent.putExtra(INTEREST_MAC, addColonsToMACAddress(macAddressNoColons));

                sendBroadcast(interestConnectGetServicesReceivedIntent);
            }
            /*
            else if (interestName.contains("connectionStatus")) {

                Log.d(TAG, "we got an interest to get the connection status of a device");

                String macAddressNoColons = interestName.substring(interestName.lastIndexOf('/') - 29,
                        interestName.lastIndexOf('/') - 17);
                int receivedSequenceNumber = Integer.parseInt(interestName.substring(interestName.lastIndexOf('/') + 1));

                if (receivedSequenceNumbers.size() > 0 && receivedSequenceNumbers.size() >= MAX_SEQUENCE_QUEUE_SIZE) {
                    receivedSequenceNumbers.remove();
                    receivedSequenceNumbers.add(receivedSequenceNumber);
                } else {
                    receivedSequenceNumbers.add(receivedSequenceNumber);
                }

                Log.d(TAG, "adding " + Integer.toString(receivedSequenceNumber) + " to queue");

                Log.d(TAG, "sequence number of connection status interest: " + interestName.substring(interestName.lastIndexOf('/') + 1));
                Log.d(TAG, "macAddressNoColons: " + macAddressNoColons);
            }
            */

        }
    };

    private final OnRegisterSuccess OnPrefixRegisterSuccessConnect = new OnRegisterSuccess() {
        @Override
        public void onRegisterSuccess(Name prefix, long registeredPrefixId) {
            Log.d(TAG, "successfully registered data prefix: " + prefix);
        }
    };

    private final OnRegisterFailed OnPrefixRegisterFailedConnect = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            Log.d(TAG, "we failed to register the data prefix: " + prefix);
        }
    };

    private void initializeKeyChain() {
        Log.d(TAG, "initializing keychain");
        MemoryIdentityStorage identityStorage = new MemoryIdentityStorage();
        MemoryPrivateKeyStorage privateKeyStorage = new MemoryPrivateKeyStorage();
        IdentityManager identityManager = new IdentityManager(identityStorage, privateKeyStorage);
        keyChain = new KeyChain(identityManager);
        keyChain.setFace(face);
    }

    private void setCommandSigningInfo() {
        Log.d(TAG, "setting command signing info");
        Name defaultCertificateName;
        try {
            defaultCertificateName = keyChain.getDefaultCertificateName();
        } catch (SecurityException e) {
            Log.d(TAG, "unable to get default certificate name");

            // NOTE: This is based on apps-NDN-Whiteboard/helpers/Utils.buildTestKeyChain()...
            Name testIdName = new Name("/test/identity");
            try {
                defaultCertificateName = keyChain.createIdentityAndCertificate(testIdName);
                keyChain.getIdentityManager().setDefaultIdentity(testIdName);
                Log.d(TAG, "created default ID: " + defaultCertificateName.toString());
            } catch (SecurityException e2) {
                defaultCertificateName = new Name("/bogus/certificate/name");
            }
        }
        face.setCommandSigningInfo(keyChain, defaultCertificateName);
    }

    public void expressInterest(String dataName) {

        Log.d(TAG + " actual string data ", dataName);

        Name name = new Name(dataName);

        Log.d(TAG, name.toString());

        expressInterest(name);
    }

    private void expressInterest(Name dataName) {
        Log.d(TAG, "expressing interest for " + dataName.toString());

        class OneShotTask implements Runnable {
            Name name;

            OneShotTask(Name n) {
                name = n;
            }

            public void run() {
                try {
                    Interest interest = new Interest(name);

                    if (name.toString().equals("/testApp/connectionStatus")) {
                        interest.setInterestLifetimeMilliseconds(12000);
                    }

                    interest.setMustBeFresh(true);

                    face.expressInterest(interest, OnReceivedData,
                            OnInterestTimeout, OnInterestNack);

                } catch (IOException e) {
                    Log.d(TAG, "failure when responding to data interest: " + e.toString());
                }
            }
        }
        Thread t = new Thread(new OneShotTask(dataName));
        t.start();

    }

    private final Thread networkThread = new Thread(new Runnable() {

        @Override
        public void run () {
            Log.d(TAG, "network thread started");
            try {
                face = new Face("localhost");
                initializeKeyChain();
                setCommandSigningInfo();
                registerDataPrefix();
                //setUpChronoSync();
                //doApplicationSetup();
            } catch (Exception e) {
                //raiseError("error during network thread initialization",
                //ErrorCode.OTHER_EXCEPTION, e);
            }
            while (!alreadyRunning) {
                try {
                    face.processEvents();
                    Thread.sleep(100); // avoid hammering the CPU
                } catch (IOException e) {
                    //raiseError("error in processEvents loop", ErrorCode.NFD_PROBLEM, e);
                } catch (Exception e) {
                    //raiseError("error in processEvents loop", ErrorCode.OTHER_EXCEPTION, e);
                }
            }
            doFinalCleanup();
            //handleAnyRaisedError();
            Log.d(TAG, "network thread stopped");


        }
    });

    private final IBinder mBinder = new NFDService.LocalBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        NFDService getService() {
            return NFDService.this;
        }
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(NFD_STOPPED);
        filter.addAction(FACE_CLOSED);
        filter.addAction(INTEREST_RECEIVED);
        filter.addAction(DATA_RECEIVED);
        filter.addAction(INTEREST_SENT);
        filter.addAction(DATA_SENT);
        filter.addAction(INTEREST_CONNECT_GET_SERVICES_RECEIVED);
        filter.addAction(INTEREST_DISCOVER_RECEIVED);
        filter.addAction(INTEREST_DISCONNECT_RECEIVED);
        return filter;
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void doFinalCleanup() {
        Log.d(TAG, "cleaning up and resetting service...");
        if (face != null) face.shutdown();
        face = null;
        Log.d(TAG, "service cleanup/reset complete");
    }

    String addColonsToMACAddress(String address) {
        StringBuilder sb = new StringBuilder(MAC_ADDRESS_LENGTH);

        sb.append(address.substring(0,2));
        sb.append(':');
        sb.append(address.substring(2,4));
        sb.append(':');
        sb.append(address.substring(4,6));
        sb.append(':');
        sb.append(address.substring(6,8));
        sb.append(':');
        sb.append(address.substring(8,10));
        sb.append(':');
        sb.append(address.substring(10,12));

        return sb.toString();
    }

    private final OnData OnReceivedData = new OnData() {
        @Override
        public void onData(Interest interest, Data data) {
            Name name = data.getName();
            //byte[] content = data.getContent().getImmutableArray();
            String content = data.getContent().toString();
            Log.d(TAG, "received data for " + name);
            //handleApplicationData(content);

            Intent intent = new Intent(DATA_RECEIVED);
            intent.putExtra("STRING_DATA", content);
            sendBroadcast(intent);
        }
    };

    private final OnTimeout OnInterestTimeout = new OnTimeout() {
        @Override
        public void onTimeout(Interest interest) {
            Name name = interest.getName();
            sendBroadcast(new Intent(INTEREST_TIMEOUT));
            Log.d(TAG, "timed out waiting for " + name);
            //expressDataInterest(name);
        }
    };

    private final OnNetworkNack OnInterestNack = new OnNetworkNack() {
        @Override
        public void onNetworkNack(Interest interest, NetworkNack networkNack) {
            Name name = interest.getName();
            sendBroadcast(new Intent(INTEREST_NACK));
            Log.d(TAG, "received NACK for " + name);
        }
    };
}