package com.edwardlu.bleapp3;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.AdvertiseData;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "Main";

    // constant for the location permissions request broadcast
    private final int REQUEST_COARSE_LOCATION = 11;

    // constant to define the scanning period
    private static final long SCAN_PERIOD = 2000;

    // bluetoothAdapter to enable bluetooth, start LE scanning
    private BluetoothAdapter bluetoothAdapter;

    // listAdapter to act as bridge between discovered device data and UI's listview
    private ListAdapter mLeDeviceListAdapter;

    // buttons for user interaction
    private Button enableBluetoothButton;
    private Button enableLocationServicesButton;
    private Button startBleScanButton;
    private Button moreInfoForBleDeviceButton;

    // textViews to display selected device information
    private TextView selectedDeviceName;
    private TextView selectedDeviceAddress;
    private TextView scanRecordInfo;

    // help with BLE scanning
    private boolean scanning;
    private Handler handler;

    // ArrayList to hold scanRecord information for discovered devices
    private ArrayList<byte[]> discoveredDevicesScanRecords;

    // ArrayList to hold display information for discovered devices
    private ArrayList<String> discoveredDevices;

    // ArrayList to hold discovered bluetooth device objects
    private ArrayList<BluetoothDevice> discoveredDevicesObjects;

    // HashSet to prevent duplicate entries into the ArrayList
    private HashSet<String> discoveredUniqueDevices;

    // arrayAdapter to display elements of discoveredDevices arrayList
    private ArrayAdapter<String> arrayAdapter;

    // listview for displaying discovered devices
    private ListView discoveredDevicesList;

    // bleService object and broadcastReceiver and serviceConnection, as well as booleans to detect
    // state for the BLEService
    /***********************************************************************/

    // object of bleService to do BLE connections
    private BLEService bleService;

    String deviceAddress;

    boolean connected = false;
    boolean connecting = false;
    boolean discoveringServices = false;

    private boolean bleServiceIsBound = false;

    String lastServiceInfoFetched = "";

    private final BroadcastReceiver bleStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BLEService.ACTION_DISCOVERING_SERVICES)) {

                Log.d(TAG, "we got the discovering_services intent from bleService");

                connected = false;
                connecting = false;
                discoveringServices = true;

                String macAddressNoColons = intent.getExtras().getString(BLEService.MAC_ADDRESS_NO_COLONS);

                nfdService.expressInterest("testApp/connectionStatus/discoveringServices");

                //nfdService.sendStatusDataToNFDFace("discoveringServices", getString(R.string.baseName)
                //        + "/connect/" + macAddressNoColons + "/connectionStatus");

            }
            else if (intent.getAction().equals(BLEService.ACTION_CONNECTED)) {

                Log.d(TAG, "we got the action_connected intent from bleService");

                connected = true;
                connecting = false;
                discoveringServices = false;

                String[] connectionInformation = intent.getStringArrayExtra(BLEService.CONNECTION_INFORMATION);

                String serviceInfo = "Services info: \n" +
                        connectionInformation[0];

                Log.d(TAG, connectionInformation[0]);

                String macAddressNoColons = connectionInformation[1];

                nfdService.sendDataToNFDFace(serviceInfo, "/localhop/ble/connect/" + macAddressNoColons + "/getServices");

                nfdService.expressInterest("/testApp/connectionStatus/connected");

                //nfdService.sendStatusDataToNFDFace("connected", getString(R.string.baseName)
                //        + "/connect/" + macAddressNoColons + "/connectionStatus");

            }
            else if (intent.getAction().equals(BLEService.ACTION_DISCONNECTED)) {

                Log.d(TAG, "we got the action_disconnected intent from bleService");

                connected = false;
                connecting = false;
                discoveringServices = false;

                String macAddressNoColons = intent.getExtras().getString(BLEService.MAC_ADDRESS_NO_COLONS);

                //nfdService.sendStatusDataToNFDFace("disconnected", getString(R.string.baseName)
                //        + "/connect/" + macAddressNoColons + "/connectionStatus");

                nfdService.expressInterest("/testApp/connectionStatus/disconnected");

            }
            else if (intent.getAction().equals(BLEService.ACTION_CONNECTING)) {

                Log.d(TAG, "we got the action_connecting intent from bleService");

                connected = false;
                connecting = true;
                discoveringServices = false;

                String macAddressNoColons = intent.getExtras().getString(BLEService.MAC_ADDRESS_NO_COLONS);

                //nfdService.sendStatusDataToNFDFace("connecting", getString(R.string.baseName)
                //        + "/connect/" + macAddressNoColons + "/connectionStatus");

                nfdService.expressInterest("/testApp/connectionStatus/connecting");

            }
            else if (intent.getAction().equals(BLEService.ACTION_DATA_AVAILABLE)) {
                // this is going to be code that uses the nfd service to send data regarding a characteristic
                // change to the testApp
            }
            updateUI();
        }
    };

    private final ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected for bleService got called.");

            bleService = ((BLEService.LocalBinder) service).getService();
            if (bleService.initialize()) {
                if (bleService.connect(deviceAddress)) {
                    Log.d(TAG, "succeeded in starting connection attempt");
                    //upgradeState(STATE_CONNECTING);
                }
            } else {
                Log.d(TAG, "The initialize for bleService failed.");
            }
            bleServiceIsBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bleServiceIsBound = false;
            bleService = null;
            Log.d(TAG, "ble service was disconnected");
        }
    };
    /********************************************************/

    // nfdService object and broadcastReceiver and serviceConnection for the NFDService
    /*****************************************************/
    private NFDService nfdService;

    private final BroadcastReceiver nfdStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(NFDService.INTEREST_DISCOVER_RECEIVED)) {

                scanLeDevice();

            }
            else if (action.equals(NFDService.INTEREST_CONNECT_GET_SERVICES_RECEIVED)) {

                deviceAddress = intent.getExtras().getString(NFDService.INTEREST_MAC);

                Log.d("nfdStatusListener", deviceAddress);

                if (!connected) {
                    if (!bleServiceIsBound) {
                        Intent bleIntent = new Intent(MainActivity.this, BLEService.class);
                        boolean test = bindService(bleIntent, bleServiceConnection, BIND_AUTO_CREATE);
                        if (test) {
                            Log.d(TAG, "bindService for bleService was successful");
                        } else {
                            Log.d(TAG, "bindService for bleService was not successful");
                        }
                    }
                    else {
                        if (deviceAddress != null && deviceAddress != "") {
                            Log.d(TAG, "attempting to connect to " + deviceAddress + "...");
                            bleService.connect(deviceAddress);
                        } else {
                            Log.d(TAG, "no device address to connect to!");
                        }
                    }
                }
                updateUI();

            }
            else if (action.equals(NFDService.INTEREST_DISCONNECT_RECEIVED)) {
                Log.d(TAG, "we got an interest to disconect so yeah");

                if (bleService != null)
                    bleService.disconnect();
            }

            updateUI();
        }
    };

    private final ServiceConnection nfdServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected for nfdServiceConnection got called.");
            nfdService = ((NFDService.LocalBinder) service).getService();

            nfdService.createNFDTunnel();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            nfdService = null;
        }
    };
    /**********************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerReceiver(nfdStatusListener, NFDService.getIntentFilter());

        registerReceiver(bleStatusListener, BLEService.getIntentFilter());


        // initializing handler, which is used to limit the BLE scan to SCAN_PERIOD milliseconds
        handler = new Handler();

        // initializing data structures that hold discovered devices
        discoveredDevices = new ArrayList<String>();
        discoveredUniqueDevices = new HashSet<String>();
        discoveredDevicesObjects = new ArrayList<BluetoothDevice>();
        discoveredDevicesScanRecords = new ArrayList<byte[]>();

        // initializing listView that displays discovered devices
        discoveredDevicesList = (ListView) findViewById(R.id.discoveredDevicesList);

        // getting default bluetooth Adapter for enabling bluetooth / doing LE scan
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // initialize the textviews to hold selected device
        selectedDeviceName = (TextView) findViewById(R.id.deviceName);
        selectedDeviceAddress = (TextView) findViewById(R.id.deviceAddress);
        scanRecordInfo = (TextView) findViewById(R.id.scanRecordInfo);

        if (bluetoothAdapter == null) {
            // some way to exit the program? or notify the user?
        }

        // Turn bluetooth on
        enableBluetoothButton = (Button) findViewById(R.id.enableBluetooth);
        enableBluetoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.enable();
                }
                updateUI();
            }
        });

        // Get the user's permission for location services for the app
        enableLocationServicesButton = (Button) findViewById(R.id.enableLocation);
        enableLocationServicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLocationPermission();
                updateUI();
            }
        });

        // button to scan for BLE devices for a specific amount of time
        startBleScanButton = (Button) findViewById(R.id.scanBLE);
        startBleScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanLeDevice();

                updateUI();
            }
        });

        // button to get more information about BLE Device
        moreInfoForBleDeviceButton = (Button) findViewById(R.id.moreInfoButton);
        moreInfoForBleDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startConnectIntent = new Intent(MainActivity.this, ConnectActivity.class);

                Log.d(TAG, "more info button got clicked, device address: " + selectedDeviceAddress.getText().toString());

                Log.d(TAG, "more info button got clicked, full info sent: " + selectedDeviceAddress.getText().toString() + selectedDeviceName.getText().toString());

                startConnectIntent.putExtra(ConnectActivity.DEVICE_INFO,
                        selectedDeviceAddress.getText().toString() + selectedDeviceName.getText().toString());

                startActivity(startConnectIntent);
            }
        });

        // arrayAdapter for dispalying the list of discovered devices on screen
        arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, discoveredDevices);
        // Set The Adapter
        discoveredDevicesList.setAdapter(arrayAdapter);

        // register onClickListener to handle click events on each item
        discoveredDevicesList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            // argument position gives the index of item which is clicked
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                String selectedDevice = discoveredDevices.get(position);
                //Toast.makeText(getApplicationContext(),
                //        "Address of device selected: " + selectedDevice.substring(selectedDevice.lastIndexOf(' ') + 1),
                //        Toast.LENGTH_LONG).show();
                selectedDeviceName.setText(selectedDevice.substring(0, selectedDevice.indexOf('\n')));
                selectedDeviceAddress.setText(selectedDevice.substring(selectedDevice.lastIndexOf('\n') + 1));

                //deviceAddress = selectedDevice.substring(selectedDevice.lastIndexOf('\n') + 1);

                scanRecordInfo.setText(parseScanRecord(discoveredDevicesScanRecords.get(position)));

                updateUI();
            }
        });

        // starts the nfd service
        Intent nfdIntent = new Intent(MainActivity.this, NFDService.class);
        boolean test = bindService(nfdIntent, nfdServiceConnection, BIND_AUTO_CREATE);
        if (test) {
            Log.d(TAG, "bindService for nfdService was successful");
        } else {
            Log.d(TAG, "bindService for nfdService was not successful");
        }

        // update UI so buttons have correct text, devices are properly displayed, etc
        updateUI();
    }

    // function to make calling scanLeDevice with checking the scanning variable easier
    private void scanLeDevice () {
        // this clears all the information regarding devices found during a previous scan
        if (!scanning) {
            discoveredDevicesScanRecords.clear();
            discoveredUniqueDevices.clear();
            discoveredDevices.clear();
            discoveredDevicesObjects.clear();
        }
        // turns scan off if currently scanning, turns scan on if not scanning
        if (scanning) {
            scanLeDevice(false);
        } else {
            scanLeDevice(true);
        }
    }

    // Stops scanning after SCAN_PERIOD milliseconds to save battery
    private void scanLeDevice ( final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scanning = false;
                    bluetoothAdapter.stopLeScan(mLeScanCallback);
                    Log.d(TAG, "stopped LE scan after timer ran out");

                    // after scan finishes, sends the data to NFD face
                    if (nfdService != null) {
                        Log.d(TAG, "nfdService wasn't null");
                        nfdService.sendDataToNFDFace(stringArrayToString(discoveredDevices), "/localhop/ble/discover");
                        //Log.d(TAG, stringArrayToString(discoveredDevices));
                    } else {
                        Log.d(TAG, "nfdService was null");
                    }

                    updateUI();
                }
            }, SCAN_PERIOD);

            scanning = true;
            bluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            scanning = false;
            bluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        updateUI();
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     final byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String deviceInfo = "Device name: " + device.getName() + "\nDevice Address:\n" + device.getAddress();
                            if (!discoveredUniqueDevices.contains(deviceInfo)) {

                                discoveredDevicesScanRecords.add(scanRecord);
                                discoveredDevicesObjects.add(device);
                                discoveredUniqueDevices.add(deviceInfo);
                                arrayAdapter.add(deviceInfo);

                            }
                        }
                    });
                }
            };

    // this function is called throughout the program to update the UI; change button text, etc
    void updateUI() {
        enableBluetoothButton.setText(
                bluetoothAdapter.isEnabled() ? "Bluetooth Already Enabled" : "Enable Bluetooth"
        );

        enableLocationServicesButton.setText(
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED ? "Location Services Already Enabled" : "Enable Location Services"
        );

        startBleScanButton.setText(
                scanning ? "Cancel Current Scan" : "Scan for BLE Devices"
        );

        if (selectedDeviceName.getText().toString().equals("No Device Selected")) {
            moreInfoForBleDeviceButton.setEnabled(false);
        } else {
            moreInfoForBleDeviceButton.setEnabled(true);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            startBleScanButton.setEnabled(true);
        } else {
            startBleScanButton.setEnabled(false);
        }

    }

    // function for requesting location services permission from the user
    protected boolean requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "We didn't have the user's permission to use location services yet...");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COARSE_LOCATION);
        } else {
            Log.d(TAG, "We DID have the user's permission to use location services");
            return true;
        }

        return false;
    }

    // function that checks the result of asking the user for location services permission
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "We got the user's permission to do location services....");
                } else {
                    Log.d(TAG, "We didn't get user's permission to do location services...");
                }
                break;
            }
        }
    }

    // overrode callback so that if the user leaves app and disables bluetooth or something else,
    // the ui can be updated properly
    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "We resumed the activity");

        updateUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (nfdServiceConnection != null)
            unbindService(nfdServiceConnection);

        unregisterReceiver(nfdStatusListener);

        if (bleServiceIsBound && bleServiceConnection != null)
            unbindService(bleServiceConnection);

        unregisterReceiver(bleStatusListener);

    }

    String stringArrayToString(ArrayList<String> strings) {

        String list = "";

        for (String s: strings) {
            list += s + "\n";
        }

        return list;
    }

    // Bluetooth Spec V4.0 - Vol 3, Part C, section 8
    private static String parseScanRecord(byte[] scanRecord) {
        StringBuilder output = new StringBuilder();
        int i = 0;
        while (i < scanRecord.length) {
            int len = scanRecord[i++] & 0xFF;
            if (len == 0) break;
            switch (scanRecord[i] & 0xFF) {
                case 0x01:
                    output.append("\n «Flags»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x02:
                    output.append("\n «Incomplete List of 16-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x03:
                    output.append("\n «Complete List of 16-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x04:
                    output.append("\n «Incomplete List of 32-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x05:
                    output.append("\n «Complete List of 32-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x06:
                    output.append("\n «Incomplete List of 128-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x07: // list of uuid's
                    output.append("\n «Complete List of 128-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x08:
                    output.append("\n «Shortened Local Name»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x09:
                    output.append("\n «Complete Local Name»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                // https://www.bluetooth.org/en-us/specification/assigned-numbers/generic-access-profile
                case 0x0A: // Tx Power
                    output.append("\n «Tx Power Level»: ").append(scanRecord[i+1]);
                    break;
                case 0xFF: // Manufacturer Specific data (RFduinoBLE.advertisementData)
                    output.append("\n «Manufacturer Specific Data»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i + 3, len));

                    String ascii = HexAsciiHelper.bytesToAsciiMaybe(scanRecord, i + 3, len);
                    if (ascii != null) {
                        output.append(" (\"").append(ascii).append("\")");
                    }
                    break;
            }
            i += len;
        }
        return output.toString();
    }



}
