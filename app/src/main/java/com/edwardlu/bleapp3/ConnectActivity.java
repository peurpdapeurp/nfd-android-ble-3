package com.edwardlu.bleapp3;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ConnectActivity extends AppCompatActivity {

    private final String TAG = "Connect Activity";

    public static String DEVICE_INFO = "DEVICE_INFO";

    private String deviceInfo;
    private String deviceAddress;
    private String deviceName;

    private final int MAC_ADDRESS_LENGTH = 17;

    // references to ui elements
    private TextView deviceInfoDisplay;
    private Button connectButton;
    private TextView connectionStatus;
    private TextView servicesInfo;

    // object of bleService to do BLE connections
    private BLEService bleService;

    // object of nfdService to do NFD related things
    private NFDService nfdService;

    boolean connected = false;
    boolean connecting = false;
    boolean discoveringServices = false;

    private boolean bleServiceIsBound = false;

    private final BroadcastReceiver bleStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BLEService.ACTION_DISCOVERING_SERVICES)) {
                connected = false;
                connecting = false;
                discoveringServices = true;
            }
            else if (intent.getAction().equals(BLEService.ACTION_CONNECTED)) {
                connected = true;
                connecting = false;
                discoveringServices = false;

                servicesInfo.setText("Services info: \n" +
                                            intent.getStringArrayExtra(BLEService.CONNECTION_INFORMATION)[0]);
            }
            else if (intent.getAction().equals(BLEService.ACTION_DISCONNECTED)) {
                connected = false;
                connecting = false;
                discoveringServices = false;
            }
            else if (intent.getAction().equals(BLEService.ACTION_CONNECTING)) {

                connected = false;
                connecting = true;
                discoveringServices = false;
            }
            //else if (intent.getAction().equals(BLEService.DISCOVERED_SERVICE_UUIDS)) {
            //    servicesInfo.setText(
            //            "Services info: \n" +
            //                    intent.getStringExtra(BLEService.UUID_LIST));
            //}
            else if (intent.getAction().equals(BLEService.ACTION_DATA_AVAILABLE)) {
                // this is going to be code that uses the nfd service to send data regarding a characteristic
                // change to
            }
            updateUI();
        }
    };

    private final ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected for bleService got called.");
            Log.d(TAG, "device address: " + deviceAddress);
            Log.d(TAG, "device name: " + deviceName);
            bleService = ((BLEService.LocalBinder) service).getService();
            if (bleService.initialize()) {
                if (bleService.connect(deviceAddress)) {
                    Log.d(TAG, "succeeded in starting connection attempt");
                    //upgradeState(STATE_CONNECTING);
                }
            } else {
                Log.d(TAG, "The initialize for bleService failed.");
            }
            bleServiceIsBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bleServiceIsBound = false;
            bleService = null;
            Log.d(TAG, "ble service was disconnected");
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        registerReceiver(bleStatusListener, BLEService.getIntentFilter());

        deviceInfo = getIntent().getExtras().getString(DEVICE_INFO);
        Log.d(TAG, "deviceInfo: " + deviceInfo);
        deviceAddress = deviceInfo.substring(0, MAC_ADDRESS_LENGTH);
        deviceName = deviceInfo.substring(MAC_ADDRESS_LENGTH);

        deviceInfoDisplay = (TextView) findViewById(R.id.deviceInfoDisplay);
        deviceInfoDisplay.setText(deviceName + "\nDevice Address: " + deviceAddress);

        connectButton = (Button) findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!connected) {
                    if (!bleServiceIsBound) {
                        Intent bleIntent = new Intent(ConnectActivity.this, BLEService.class);
                        boolean test = bindService(bleIntent, bleServiceConnection, BIND_AUTO_CREATE);
                        if (test) {
                            Log.d(TAG, "bindService for bleService was successful");
                        } else {
                            Log.d(TAG, "bindService for bleService was not successful");
                        }
                    }
                    else {
                        bleService.connect(deviceAddress);
                    }
                }
                else {
                    if (bleService != null)
                        bleService.disconnect();
                }
                updateUI();
            }
        });

        connectionStatus = (TextView) findViewById(R.id.connectionStatus);
        servicesInfo = (TextView) findViewById(R.id.servicesInfo);

        updateUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bleServiceIsBound && bleServiceConnection != null)
            unbindService(bleServiceConnection);

        unregisterReceiver(bleStatusListener);
    }

    void updateUI() {

        if (connected && !connecting) {
            connectButton.setText("Disconnect");
            connectionStatus.setText("Connection Status: Connected");
        }
        if (!connected && !connecting) {
            connectButton.setText("Connect");
            connectionStatus.setText("Connection Status: Disconnected");
        }

        if (connecting) {
            connectButton.setEnabled(false);
            connectionStatus.setText("Connection Status: Connecting...");
            connectButton.setText("Connecting...");
        }

        if (discoveringServices) {
            connectButton.setEnabled(false);
            connectionStatus.setText("Connection Status: Discovering services...");
            connectButton.setText("Discovering services");
        }

        if (!connecting && !discoveringServices) {
            connectButton.setEnabled(true);
        }
    }
}
